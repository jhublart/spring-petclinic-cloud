
# SPRING PETCLINIC

## Description
Spring PetClinic is a Spring Boot application designed with a microservices architecture. It showcases the use of several technologies and practices in software development.

### Microservices:
- **api-gateway**: Serves as the entry point for all requests coming into the system, routing them to the appropriate microservice.
- **customers**: Manages customer information and interactions.
- **vets**: Handles data and operations related to veterinary professionals.
- **visits**: Manages appointments and visit records of pets.

### Monitoring Services:
- **Prometheus**: An open-source monitoring system with a focus on reliability and simplicity, collecting metrics from monitored services.
- **Grafana**: A platform for visualization and analytics, displaying the data collected by Prometheus.

## Architecture
The application utilizes a GitLab CI pipeline for building and deploying using Kubernetes, a powerful open-source system for automating deployment, scaling, and management of containerized applications.

![Architecture](./docs/architecture.png)

### CI Pipeline
Each service's CI pipeline is divided into three stages:
- **Build**: Compiles the application, creates Docker images, and runs unit tests. Docker is a set of platform-as-a-service products that use OS-level virtualization to deliver software in packages called containers.
- **Push**: Uploads the Docker images to Docker Hub, a service provided by Docker for finding and sharing container images with your team.
- **Deploy**: Uses Helm, a package manager for Kubernetes, to deploy the application in a Kubernetes cluster.

### Helm
Located in the root directory, the Helm meta-chart enables deployment of the entire application with a single command:
```bash
helm install spring-petclinic helm/spring-petclinic
```

## User Interface
Screenshot of the running application:
![Application](./docs/application-screenshot.png)

## Grafana Monitoring
Configured with a dashboard for displaying application metrics, Grafana provides insights into the application's performance and health.

![Grafana](./docs/grafana-custom-metrics-dashboard.png)

## Deploying in a Kubernetes Cluster

### Prerequisites
- Access to a Kubernetes cluster (kubeconfig file).
- Docker Hub account for storing and managing container images.
- MySQL database with endpoint and credentials.
- Configured GitLab runner with Docker, Java, Maven, and kubectl for container and Kubernetes interactions.

### Configuration
Set up the following GitLab environment variables before deployment:
- `DB_PASSWORD_CUSTOMERS`: Password for the customers service database.
- `DB_PASSWORD_VETS`: Password for the vets service database.
- `DB_PASSWORD_VISITS`: Password for the visits service database.
- `DOCKER_USERNAME`: Docker Hub username.
- `DOCKER_PASSWORD`: Docker Hub password.
- `KUBE_CONFIG`: Kubeconfig file for Kubernetes cluster access.

### Deployment
Deploy the application by manually triggering the pipeline in GitLab or by committing to the master branch.
